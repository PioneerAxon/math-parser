#include<stdio.h>
#include<prelexer.h>
#include<lexer.h>
#include<parser.h>
#define MAIN_C_MAX_BUFF 4096

int main()
{
	gchar str[MAIN_C_MAX_BUFF];
	ParserState * ps;
	scanf ("%[^\n]",str);
	ps = p_create_parser (str);
	p_parse (ps);
	p_destroy_parser (ps);
	return 0;
}
